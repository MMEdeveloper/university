<?php

use Illuminate\Database\Seeder;

use App\Faculty;
use App\Classroom;
use App\Student;
use App\Teacher;
use App\Subject;
use App\Group;
use App\GroupTeacher;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(Faculty::class, 5)->create();
        // factory(Subject::class, 10)->create();
        factory(Group::class, 20)->create();
        factory(Student::class, 100)->create();
        factory(Classroom::class, 30)->create();
        factory(Teacher::class, 40)->create();
        factory(GroupTeacher::class, 10)->create();
    }
}
