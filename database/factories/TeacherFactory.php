<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Teacher;
use Faker\Generator as Faker;
use App\Subject;

$factory->define(Teacher::class, function (Faker $faker) {
    return [
        'subject_id' => factory(Subject::class),
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName
    ];
});
