<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Classroom;
use Faker\Generator as Faker;
use App\Faculty;
use App\Subject;

$factory->define(Classroom::class, function (Faker $faker) {
    return [
        'number' => $faker->numberBetween($min = 101, $max = 499),
        'faculty_id' => factory(Faculty::class), 
        'subject_id' => factory(Subject::class)
    ];
});
