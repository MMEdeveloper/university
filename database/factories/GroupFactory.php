<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Group;
use Faker\Generator as Faker;
use App\Faculty;

$factory->define(Group::class, function (Faker $faker) {
    return [
        'number' => $faker->bothify('###?'),
        'name' => $faker->sentence,
        'faculty_id' => factory(Faculty::class)
    ];
});
