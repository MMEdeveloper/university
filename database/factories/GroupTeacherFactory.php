<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\GroupTeacher;
use Faker\Generator as Faker;
use App\Group;
use App\Teacher;

$factory->define(GroupTeacher::class, function (Faker $faker) {
    return [
        'group_id' => factory(Group::class),
        'teacher_id' => factory(Teacher::class)   
    ];
});
