<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use App\Group;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'group_id' => factory(Group::class),
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
    ];
});
