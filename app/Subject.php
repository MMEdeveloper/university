<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Classroom;
use App\Teacher;

class Subject extends Model
{
    protected $fillable = [ 'name' ];

    public function classrooms()
    {
        return $this->hasMany(Classromm::class);
    }

    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }
}
