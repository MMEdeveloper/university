<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Classroom;
use App\Group;

class Faculty extends Model
{
    protected $fillable = [ 'name' ];

    public function classrooms()
    {
        return $this->hasMany(Classroom::class);
    }
    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
