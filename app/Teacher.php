<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Subject;
use App\GroupTeacher;
 
class Teacher extends Model
{
    protected $fillable = [ 'first_name', 'last_name', 'subject_id' ];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
    public function groupTeachers()
    {
        return $this->hasMany(GroupTeacher::class);
    }
}
