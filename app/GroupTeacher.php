<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Group;
use App\Teacher;

class GroupTeacher extends Model
{
    protected $fillable = [ 'group_id', 'teacher_id' ];

    public function groups()
    {
        return $this->belongsTo(Group::class);
    }
    public function teachers()
    {
        return $this->belongsTo(Teacher::class);
    }
}
