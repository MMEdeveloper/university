<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Faculty;
use App\Student;
use App\GroupTeacher;

class Group extends Model
{
    protected $fillable = [ 'number', 'name', 'faculty_id' ];

    public function faculty()
    {
        return $this->belongsTo(Faculty::class);
    }
    public function students()
    {
        return $this->hasMany(Student::class);
    }
    public function groupTeachers()
    {
        return $this->hasMany(GroupTeacher::class);
    }
}
