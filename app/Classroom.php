<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Faculty;
use App\Subject;

class Classroom extends Model
{
    protected $fillable = [ 'number', 'faculty_id', 'subject_id' ];

    public function faculty() 
    {
        return $this->belongsTo(Faculty::class);
    }
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}