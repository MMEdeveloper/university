<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Group;

class Student extends Model
{
    protected $fillable = [ 'first_name', 'last_name', 'group_id' ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
