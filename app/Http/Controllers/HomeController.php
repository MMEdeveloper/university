<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faculty;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $faculties = Faculty::all();

        return view('admin.faculty.faculty', ['faculties' => $faculties]);
        
    }
}
