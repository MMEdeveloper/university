<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GroupTeacher;

class GroupTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupTeachers = GroupTeacher::all();

        return view('admin.group_teacher.groupTeacher', ['groupTeachers' => $groupTeachers]);                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        GroupTeacher::create(request()->validate([
            'group_id' => 'required',
            'teacher_id' => 'required'
        ]));

        return redirect('admin/group-teachers'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groupTeacher = GroupTeacher::findOrFail($id);

        return view('admin.group_teacher.editGroupTeacher', ['groupTeacher' => $groupTeacher]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        GroupTeacher::findOrFail($id)->update([
            'group_id' => $request->group_id,
            'teacher_id' => $request->teacher_id
        ]);

        return redirect('admin/group-teachers'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GroupTeacher::findOrFail($id)->delete();

        return redirect('admin/group-teachers'); 
    }
}
