<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="/" class="brand-link">
    <img src="../../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">University</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="../../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Mirshod Mustafoyev</a>
      </div>
    </div>


    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
     
        <li class="nav-item">
          <a href="/admin/faculties" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Faculties</p>
          </a>
        </li>
        
        <li class="nav-item">
          <a href="/admin/subjects" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Subjects</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/admin/classrooms" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Classrooms</p>
          </a>
        </li>
        
        <li class="nav-item">
          <a href="/admin/groups" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Groups</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/admin/teachers" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Teachers</p>
          </a>
        </li>
        
        <li class="nav-item">
          <a href="/admin/students" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Students</p>
          </a>
        </li>
        
        <li class="nav-item">
          <a href="/admin/group-teachers" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Group-Teacher</p>
          </a>
        </li>
        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>