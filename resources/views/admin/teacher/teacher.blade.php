@extends('admin.dashboard')

@section('content')

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1>Create teacher</h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="/admin/teachers">

      @csrf

      <div class="card-body">
        <div class="form-teacher">
          <label for="first_name">Teacher first-name</label>
          <input 
            type="text" 
            class="form-control" 
            name="first_name" 
            id="first_name" 
            required
            value="{{ old('first_name') }}"
            placeholder="Enter teacher first_name">

          <label for="last_name">Teacher last-name</label>
          <input 
            type="text" 
            class="form-control" 
            name="last_name" 
            id="last_name" 
            required
            value="{{ old('last_name') }}"
            placeholder="Enter teacher last_name">
         
          <label for="subject_id">Teacher subject-id</label>   
          <input 
            type="text" 
            class="form-control" 
            name="subject_id" 
            id="subject_id" 
            required
            value="{{ old('subject_id') }}"
            placeholder="Enter teacher subject_id">
          </div>  
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <!-- /.card -->


  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Teachers List</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Subject Id</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($teachers as $teacher)
                      
                    <tr>
                      <td>{{ $teacher->id }}</td>
                      <td>{{ $teacher->first_name }}</td>
                      <td>{{ $teacher->last_name }}</td>
                      <td>{{ $teacher->subject_id }}</td>
                      
                      <td>
                        <a href="{{route('edit.teacher', ['id' => $teacher->id])}}">
                          <button class="btn btn-outline-success">
                            <i class="fas fa-edit"></i>
                          </button>
                        </a>
                      <td>
                        <form action="{{route('delete.teacher', ['id' => $teacher->id])}}" method="POST">
                          @csrf
                          @method('delete')
                          
                          <button type="submit" class="btn btn-outline-danger">
                            <i class="fas fa-trash"></i>
                          </button>
                        </form>
                      </td>

                    </tr>
                    
                  @endforeach
                
                </tbody>
                
              </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
@endsection
