@extends('admin.dashboard')

@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Update teacher</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="{{route('update.teacher', ['id' => $teacher->id])}}">

      @csrf
      @method('PUT')

      <div class="card-body">
        <div class="form-teacher">
          <label for="first_name">Teacher first-name</label>
          <input 
            type="text" 
            class="form-control" 
            name="first_name" 
            id="first_name" 
            required
            value="{{ $teacher->first_name }}"
            placeholder="Enter teacher first_name">
          <label for="last_name">Teacher last-name</label>
          <input 
            type="text" 
            class="form-control" 
            name="last_name" 
            id="last_name" 
            required
            value="{{ $teacher->last_name }}"
            placeholder="Enter teacher last-name">           
         
          <label for="subject_id">Teacher subject-id</label>   
          <input 
            type="text" 
            class="form-control" 
            name="subject_id" 
            id="subject_id" 
            required
            value="{{ $teacher->subject_id }}"
            placeholder="Enter teacher subject-id">

        </div>
      <!-- /.card-body -->
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
  <!-- /.card -->

@endsection
