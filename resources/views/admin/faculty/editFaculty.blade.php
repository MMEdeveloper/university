@extends('admin.dashboard')

@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Update Faculty</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="{{route('update.faculty', ['id' => $faculty->id])}}">

      @csrf
      @method('PUT')

      <div class="card-body">
        <div class="form-group">
          <label for="name">Faculty name</label>
          <input 
            type="text" 
            class="form-control" 
            name="name" 
            id="name" 
            required
            value="{{ $faculty->name }}"
            placeholder="Enter faculty name">
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
  <!-- /.card -->

@endsection
