@extends('admin.dashboard')

@section('content')

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1>Create Faculty</h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="/admin/faculties">

      @csrf

      <div class="card-body">
        <div class="form-group">
          <label for="name">Faculty name</label>
          <input 
            type="text" 
            class="form-control" 
            name="name" 
            id="name" 
            required
            value="{{ old('name') }}"
            placeholder="Enter faculty name">
        </div>   
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <!-- /.card -->


  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Faculties List</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Name</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($faculties as $faculty)   
                    <tr>                      
                      <td>{{ $faculty->id }}</td>
                      <td>{{ $faculty->name }}</td>
                      <td>
                        <a href="{{route('edit.faculty', ['id' => $faculty->id])}}">
                          <button class="btn btn-outline-success">
                            <i class="fas fa-edit"></i>
                          </button>
                        </a>
                      <td>
                        <form action="{{route('delete.faculty', ['id' => $faculty->id])}}" method="POST">
                          @csrf
                          @method('delete')
                          
                          <button type="submit" class="btn btn-outline-danger">
                            <i class="fas fa-trash"></i>
                          </button>
                        </form>
                      </td>
                    </tr>
                    
                  @endforeach
                
                </tbody>
                
              </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
@endsection
