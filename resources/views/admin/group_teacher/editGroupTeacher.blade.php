@extends('admin.dashboard')

@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Update Group - Teacher</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="{{route('update.groupTeacher', ['id' => $groupTeacher->id])}}">

      @csrf
      @method('PUT')

      <div class="card-body">
        <div class="form-groupTeacher">
          <label for="group_id">Group - Teacher group-id</label>
          <input 
            type="text" 
            class="form-control" 
            name="group_id" 
            id="group_id" 
            required
            value="{{ $groupTeacher->group_id }}"
            placeholder="Enter groupTeacher group_id">
          <label for="teacher_id">groupTeacher teacher-id</label>
          <input 
            type="text" 
            class="form-control" 
            name="teacher_id" 
            id="teacher_id" 
            required
            value="{{ $groupTeacher->teacher_id }}"
            placeholder="Enter groupTeacher teacher_id">           
         
        </div>
      <!-- /.card-body -->
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
  <!-- /.card -->

@endsection
