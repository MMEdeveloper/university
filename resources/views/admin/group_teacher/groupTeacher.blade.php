@extends('admin.dashboard')

@section('content')

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1>Create Group - Teacher Relation</h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="/admin/group-teachers">

      @csrf

      <div class="card-body">
        <div class="form-groupTeacher">
          <label for="group_id">Group - Teacher group-id</label>
          <input 
            type="text" 
            class="form-control" 
            name="group_id" 
            id="group_id" 
            required
            value="{{ old('group_id') }}"
            placeholder="Enter groupTeacher group_id">

          <label for="teacher_id">Group - Teacher teacher-id</label>
          <input 
            type="text" 
            class="form-control" 
            name="teacher_id" 
            id="teacher_id" 
            required
            value="{{ old('teacher_id') }}"
            placeholder="Enter groupTeacher teacher_id">
         
          </div>  
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <!-- /.card -->


  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Group - Teachers List</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Group Id</th>
                  <th>Teacher Id</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($groupTeachers as $groupTeacher)
                      
                    <tr>
                      <td>{{ $groupTeacher->id }}</td>
                      <td>{{ $groupTeacher->group_id }}</td>
                      <td>{{ $groupTeacher->teacher_id }}</td>
                      
                      <td>
                        <a href="{{route('edit.groupTeacher', ['id' => $groupTeacher->id])}}">
                          <button class="btn btn-outline-success">
                            <i class="fas fa-edit"></i>
                          </button>
                        </a>
                      <td>
                        <form action="{{route('delete.groupTeacher', ['id' => $groupTeacher->id])}}" method="POST">
                          @csrf
                          @method('delete')
                          
                          <button type="submit" class="btn btn-outline-danger">
                            <i class="fas fa-trash"></i>
                          </button>
                        </form>
                      </td>

                    </tr>
                    
                  @endforeach
                
                </tbody>
                
              </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
@endsection
