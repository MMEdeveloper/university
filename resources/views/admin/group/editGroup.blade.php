@extends('admin.dashboard')

@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Update group</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="{{route('update.group', ['id' => $group->id])}}">

      @csrf
      @method('PUT')

      <div class="card-body">
        <div class="form-group">
          <label for="number">group number</label>
          <input 
            type="text" 
            class="form-control" 
            name="number" 
            id="number" 
            required
            value="{{ $group->number }}"
            placeholder="Enter group number">
          <label for="name">group name</label>
          <input 
            type="text" 
            class="form-control" 
            name="name" 
            id="name" 
            required
            value="{{ $group->name }}"
            placeholder="Enter group name">           
         
          <label for="faculty_id">group faculty_id</label>   
          <input 
            type="text" 
            class="form-control" 
            name="faculty_id" 
            id="faculty_id" 
            required
            value="{{ $group->faculty_id }}"
            placeholder="Enter group faculty_id">

        </div>
      <!-- /.card-body -->
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
  <!-- /.card -->

@endsection
