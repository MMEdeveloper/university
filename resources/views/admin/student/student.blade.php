@extends('admin.dashboard')

@section('content')

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1>Create student</h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="/admin/students">

      @csrf

      <div class="card-body">
        <div class="form-student">
          <label for="first_name">student first_name</label>
          <input 
            type="text" 
            class="form-control" 
            name="first_name" 
            id="first_name" 
            required
            value="{{ old('first_name') }}"
            placeholder="Enter student first_name">

          <label for="last_name">student last_name</label>
          <input 
            type="text" 
            class="form-control" 
            name="last_name" 
            id="last_name" 
            required
            value="{{ old('last_name') }}"
            placeholder="Enter student last_name">
         
          <label for="group_id">student group_id</label>   
          <input 
            type="text" 
            class="form-control" 
            name="group_id" 
            id="group_id" 
            required
            value="{{ old('group_id') }}"
            placeholder="Enter student group_id">
          </div>  
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <!-- /.card -->


  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Students List</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Group Id</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($students as $student)
                      
                    <tr>
                      <td>{{ $student->id }}</td>
                      <td>{{ $student->first_name }}</td>
                      <td>{{ $student->last_name }}</td>
                      <td>{{ $student->group_id }}</td>
                      
                      <td>
                        <a href="{{route('edit.student', ['id' => $student->id])}}">
                          <button class="btn btn-outline-success">
                            <i class="fas fa-edit"></i>
                          </button>
                        </a>
                      <td>
                        <form action="{{route('delete.student', ['id' => $student->id])}}" method="POST">
                          @csrf
                          @method('delete')
                          
                          <button type="submit" class="btn btn-outline-danger">
                            <i class="fas fa-trash"></i>
                          </button>
                        </form>
                      </td>

                    </tr>
                    
                  @endforeach
                
                </tbody>
                
              </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
@endsection
