@extends('admin.dashboard')

@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Update student</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="{{route('update.student', ['id' => $student->id])}}">

      @csrf
      @method('PUT')

      <div class="card-body">
        <div class="form-student">
          <label for="first_name">Student first-name</label>
          <input 
            type="text" 
            class="form-control" 
            name="first_name" 
            id="first_name" 
            required
            value="{{ $student->first_name }}"
            placeholder="Enter student first_name">
          <label for="last_name">Student last-name</label>
          <input 
            type="text" 
            class="form-control" 
            name="last_name" 
            id="last_name" 
            required
            value="{{ $student->last_name }}"
            placeholder="Enter student last-name">           
         
          <label for="group_id">Student group-id</label>   
          <input 
            type="text" 
            class="form-control" 
            name="group_id" 
            id="group_id" 
            required
            value="{{ $student->group_id }}"
            placeholder="Enter student group-id">

        </div>
      <!-- /.card-body -->
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
  <!-- /.card -->

@endsection
