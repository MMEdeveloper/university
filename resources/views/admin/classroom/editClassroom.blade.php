@extends('admin.dashboard')

@section('content')

  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12">
          <h1>Update classroom</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="{{route('update.classroom', ['id' => $classroom->id])}}">

      @csrf
      @method('PUT')

      <div class="card-body">
        <div class="form-group">
          <label for="number">classroom number</label>
          <input 
            type="text" 
            class="form-control" 
            name="number" 
            id="number" 
            required
            value="{{ $classroom->number }}"
            placeholder="Enter classroom number">
         
            <label for="faculty_id">classroom faculty_id</label>   
            <input 
            type="text" 
            class="form-control" 
            name="faculty_id" 
            id="faculty_id" 
            required
            value="{{ $classroom->faculty_id }}"
            placeholder="Enter classroom faculty_id">

            <label for="subject_id">classroom subject_id</label>
            <input 
            type="text" 
            class="form-control" 
            name="subject_id" 
            id="subject_id" 
            required
            value="{{ $classroom->subject_id }}"
            placeholder="Enter classroom subject_id">           
        </div>
      <!-- /.card-body -->
      </div>
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
  <!-- /.card -->

@endsection
