@extends('admin.dashboard')

@section('content')

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1>Create classroom</h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

  <!-- general form elements -->
  <div class="card card-primary">
    <!-- form start -->
    <form method="POST" action="/admin/classrooms">

      @csrf

      <div class="card-body">
        <div class="form-group">
          <label for="number">classroom number</label>
          <input 
            type="text" 
            class="form-control" 
            name="number" 
            id="number" 
            required
            value="{{ old('number') }}"
            placeholder="Enter classroom number">
         
          <label for="faculty_id">classroom faculty_id</label>   
          <input 
            type="text" 
            class="form-control" 
            name="faculty_id" 
            id="faculty_id" 
            required
            value="{{ old('faculty_id') }}"
            placeholder="Enter classroom faculty_id">

          <label for="subject_id">classroom subject_id</label>
          <input 
            type="text" 
            class="form-control" 
            name="subject_id" 
            id="subject_id" 
            required
            value="{{ old('subject_id') }}"
            placeholder="Enter classroom subject_id">
            
          </div>  
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
  <!-- /.card -->


  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">classrooms List</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Number</th>
                  <th>Faculty_id</th>
                  <th>Subject_id</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($classrooms as $classroom)
                      
                    <tr>
                      <td>{{ $classroom->id }}</td>
                      <td>{{ $classroom->number }}</td>
                      <td>{{ $classroom->faculty_id }}</td>
                      <td>{{ $classroom->subject_id }}</td>
                      
                      <td>
                        <a href="{{route('edit.classroom', ['id' => $classroom->id])}}">
                          <button class="btn btn-outline-success">
                            <i class="fas fa-edit"></i>
                          </button>
                        </a>
                      <td>
                        <form action="{{route('delete.classroom', ['id' => $classroom->id])}}" method="POST">
                          @csrf
                          @method('delete')
                          
                          <button type="submit" class="btn btn-outline-danger">
                            <i class="fas fa-trash"></i>
                          </button>
                        </form>
                      </td>

                    </tr>
                    
                  @endforeach
                
                </tbody>
                
              </table>
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
@endsection
