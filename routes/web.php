<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::prefix('admin/classrooms')->group(function () {
    Route::get('/', 'ClassroomController@index');
    Route::post('/', 'ClassroomController@store');
    Route::get('/{id}/edit', 'ClassroomController@edit')->name('edit.classroom');
    // Route::get('/show/{id}', 'ClassroomController@show');
    Route::put('/{id}', 'ClassroomController@update')->name('update.classroom');
    Route::delete('/{id}', 'ClassroomController@destroy')->name('delete.classroom');
});
Route::prefix('admin/faculties')->group(function () {
    Route::get('/', 'FacultyController@index');
    Route::post('/', 'FacultyController@store');
    Route::get('/{id}/edit', 'FacultyController@edit')->name('edit.faculty');
    // Route::get('/show/{id}', 'FacultyController@show');
    Route::put('/{id}', 'FacultyController@update')->name('update.faculty');
    Route::delete('/{id}', 'FacultyController@destroy')->name('delete.faculty');
});

Route::prefix('admin/subjects')->group(function () {
    Route::get('/', 'SubjectController@index');
    Route::post('/', 'SubjectController@store');
    Route::get('/{id}/edit', 'SubjectController@edit')->name('edit.subject');
    Route::get('/show/{id}', 'SubjectController@show');
    // Route::put('/{id}', 'SubjectController@update');
    Route::put('/{id}', 'SubjectController@update')->name('update.subject');
    Route::delete('/{id}', 'SubjectController@destroy')->name('delete.subject');
});

Route::prefix('admin/groups')->group(function () {
    Route::get('/', 'GroupController@index');
    Route::post('/', 'GroupController@store');
    Route::get('/{id}/edit', 'GroupController@edit')->name('edit.group');
    Route::get('/show/{id}', 'GroupController@show');
    // Route::put('/{id}', 'GroupController@update');
    Route::put('/{id}', 'GroupController@update')->name('update.group');
    Route::delete('/{id}', 'GroupController@destroy')->name('delete.group');
});

Route::prefix('admin/students')->group(function () {
    Route::get('/', 'StudentController@index');
    Route::post('/', 'StudentController@store');
    Route::get('/{id}/edit', 'StudentController@edit')->name('edit.student');
    Route::get('/show/{id}', 'StudentController@show');
    // Route::put('/{id}', 'StudentController@update');
    Route::put('/{id}', 'StudentController@update')->name('update.student');
    Route::delete('/{id}', 'StudentController@destroy')->name('delete.student');
});

Route::prefix('admin/teachers')->group(function () {
    Route::get('/', 'TeacherController@index');
    Route::post('/', 'TeacherController@store');
    Route::get('/{id}/edit', 'TeacherController@edit')->name('edit.teacher');
    Route::get('/show/{id}', 'TeacherController@show');
    // Route::put('/{id}', 'TeacherController@update');
    Route::put('/{id}', 'TeacherController@update')->name('update.teacher');
    Route::delete('/{id}', 'TeacherController@destroy')->name('delete.teacher');
});

Route::prefix('admin/group-teachers')->group(function () {
    Route::get('/', 'GroupTeacherController@index');
    Route::post('/', 'GroupTeacherController@store');
    Route::get('/{id}/edit', 'GroupTeacherController@edit')->name('edit.groupTeacher');
    Route::get('/show/{id}', 'GroupTeacherController@show');
    // Route::put('/{id}', 'GroupTeacherController@update');
    Route::put('/{id}', 'GroupTeacherController@update')->name('update.groupTeacher');
    Route::delete('/{id}', 'GroupTeacherController@destroy')->name('delete.groupTeacher');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
