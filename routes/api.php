<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::prefix('admin/classrooms')->group(function () {
//     Route::get('/', 'ClassroomController@index');
//     Route::post('/', 'ClassroomController@store');
//     // Route::get('/show/{id}', 'ClassroomController@show');
//     Route::put('/{id}', 'ClassroomController@update');
//     Route::delete('/{id}', 'ClassroomController@destroy');
// });

// Route::prefix('admin/faculties')->group(function () {
//     Route::get('/', 'FacultyController@index');
//     Route::post('/', 'FacultyController@store');
//     Route::get('/show/{id}', 'FacultyController@show');
//     // Route::put('/{id}', 'FacultyController@update');
//     Route::delete('/{id}', 'FacultyController@destroy');
// });

// Route::prefix('admin/subjects')->group(function () {
//     Route::get('/', 'SubjectController@index');
//     Route::post('/', 'SubjectController@store');
//     Route::get('/show/{id}', 'SubjectController@show');
//     // Route::put('/{id}', 'SubjectController@update');
//     Route::delete('/{id}', 'SubjectController@destroy');
// });

// Route::prefix('admin/groups')->group(function () {
//     Route::get('/', 'GroupController@index');
//     Route::post('/', 'GroupController@store');
//     // Route::get('/show/{id}', 'GroupController@show');
//     Route::put('/{id}', 'GroupController@update');
//     Route::delete('/{id}', 'GroupController@destroy');
// });

// Route::prefix('admin/students')->group(function () {
//     Route::get('/', 'StudentController@index');
//     Route::post('/', 'StudentController@store');
//     // Route::get('/show/{id}', 'StudentController@show');
//     Route::put('/{id}', 'StudentController@update');
//     Route::delete('/{id}', 'StudentController@destroy');

// });


// Route::prefix('admin/teachers')->group(function () {
//     Route::get('/', 'TeacherController@index');
//     Route::get('/', 'TeacherController@index');
//     Route::post('/', 'TeacherController@store');
//     // Route::get('/show/{id}', 'TeacherController@show');
//     Route::put('/{id}', 'TeacherController@update');
//     Route::delete('/{id}', 'TeacherController@destroy');
// });

// Route::prefix('admin/group-teachers')->group(function () {
//     Route::get('/', 'GroupTeacherController@index');
//     Route::get('/', 'GroupTeacherController@index');
//     Route::post('/', 'GroupTeacherController@store');
//     // Route::get('/show/{id}', 'GroupTeacherController@show');
//     Route::put('/{id}', 'GroupTeacherController@update');
//     Route::delete('/{id}', 'GroupTeacherController@destroy');
// });